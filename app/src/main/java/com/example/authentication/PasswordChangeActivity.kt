package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.authentication.databinding.ActivityPasswordChangeBinding
import com.example.authentication.databinding.ActivityProfileBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPasswordChangeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPasswordChangeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        registerListener()
        passwordFocusListener()
    }

    private fun passwordFocusListener() {
        binding.editTextNewPassword.setOnFocusChangeListener { _, focused ->
            if (!focused) {
                binding.containerNewPassword.helperText = validPassword()
            }
        }
    }

    private fun validPassword(): String? {
        val passwordText = binding.editTextNewPassword.text.toString()
        if (passwordText.isEmpty()) {
            return "Required"
        }
        if (passwordText.length < 9) {
            return "Minimum 9 character password"
        }
        if (!passwordText.matches(".*[A-Z].*".toRegex())) {
            return "Must contain one upper-case character"
        }
        if (!passwordText.matches(".*[a-z].*".toRegex())) {
            return "Must contain one lower-case character"
        }
        if (!passwordText.matches(".*[0-9].*".toRegex())) {
            return "Must contain one number"
        }

        return null
    }

    private fun registerListener() {
        binding.buttonChange.setOnClickListener {

            binding.editTextNewPassword.clearFocus()
            binding.editTextConfirmNewPassword.clearFocus()

            val newPassword = binding.editTextNewPassword.text.toString()

            if (newPassword.isEmpty() || newPassword.length < 9) {
                binding.textViewError.text = "* Invalid Password."
                return@setOnClickListener
            }

            if (newPassword != binding.editTextConfirmNewPassword.text.toString()) {
                binding.textViewError.text = "* Password confirm failed."
                return@setOnClickListener
            }

            val validEmail = binding.containerNewPassword.helperText == null
            val validPassword = binding.containerConfirmNewPassword.helperText == null

            if (validEmail && validPassword) {
                FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                    ?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            MaterialAlertDialogBuilder(this,
                                R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen)
                                .setMessage("Password changed successfully.")
                                .setTitle("Change Password")
                                .setPositiveButton("Ok") { dialog, which ->

                                }
                                .show()
                        } else {
                            binding.textViewError.text = "* Password change failed."
                        }
                    }
            }

        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("editTextNewPassword", binding.editTextNewPassword.text.toString())
        outState.putString("passwordText", binding.editTextConfirmNewPassword.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        binding.editTextNewPassword.setText(savedInstanceState.getString("editTextNewPassword", ""))
        binding.editTextConfirmNewPassword.setText(savedInstanceState.getString("editTextConfirmNewPassword", ""))
    }

}