package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import com.example.authentication.databinding.ActivityRegistrationBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.ktx.Firebase
import java.lang.Exception

class RegistrationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegistrationBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        registerListener()
        emailFocusListener()
        passwordFocusListener()
        confirmPasswordFocusListener()

    }

    /* email validation */

    private fun emailFocusListener() {
        binding.editTextEmail.setOnFocusChangeListener { _, focused ->
            if (!focused) {
                binding.containerEmail.helperText = validEmail()
            }
        }
    }

    private fun validEmail(): String? {
        val emailText = binding.editTextEmail.text.toString()
        if (emailText.isEmpty()) {
            return "Required"
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()) {
            return "Invalid email address"
        }

        return null
    }

    /* password validation */

    private fun passwordFocusListener() {
        binding.editTextPassword.setOnFocusChangeListener { _, focused ->
            if (!focused) {
                binding.containerPassword.helperText = validPassword()
            }
        }
    }

    private fun validPassword(): String? {
        val passwordText = binding.editTextPassword.text.toString()
        if (passwordText.isEmpty()) {
            return "Required"
        }
        if (passwordText.length < 9) {
            return "Minimum 9 character password"
        }
        if (!passwordText.matches(".*[A-Z].*".toRegex())) {
            return "Must contain one upper-case character"
        }
        if (!passwordText.matches(".*[a-z].*".toRegex())) {
            return "Must contain one lower-case character"
        }
        if (!passwordText.matches(".*[0-9].*".toRegex())) {
            return "Must contain one number"
        }

        return null
    }

    /* confirm password validation */

    private fun confirmPasswordFocusListener() {
        binding.editTextConfirmPassword.setOnFocusChangeListener { _, focused ->
            if (!focused) {
                binding.containerConfirmPassword.helperText = validConfirmPassword()
            }
        }
    }

    private fun validConfirmPassword(): String? {
        val confirmPasswordText = binding.editTextConfirmPassword.text.toString()
        if (confirmPasswordText.isEmpty()) {
            return "Required"
        }
        if (confirmPasswordText != binding.editTextPassword.text.toString()) {
            return "Invalid password confirm"
        }

        return null
    }

    /* onclick */

    private fun registerListener() {

        binding.textViewRules.setOnClickListener {
            MaterialAlertDialogBuilder(this,
                R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen)
                .setMessage("By registering you agree to:\n" +
                        "• Giorgi Pilishvili will get 5/5 points in this homework.")
                .setTitle("Terms and conditions")
                .setPositiveButton("Ok") { dialog, which ->

                }
                .show()
        }

        binding.buttonSignin.setOnClickListener {

            binding.editTextEmail.clearFocus()
            binding.editTextPassword.clearFocus()
            binding.editTextConfirmPassword.clearFocus()

            val email = binding.editTextEmail.text.toString()
            val password = binding.editTextPassword.text.toString()
            val confirmPassword = binding.editTextConfirmPassword.text.toString()

            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {
                binding.textViewError.text = "* Please fill form."
                return@setOnClickListener
            } else if (!binding.checkboxRules.isChecked) {
                binding.textViewError.text = "* Please agree to the terms and conditions."
                return@setOnClickListener
            }

            val validEmail = binding.containerEmail.helperText == null
            val validPassword = binding.containerPassword.helperText == null
            val validConfirmPassword = binding.containerConfirmPassword.helperText == null

            if (validEmail && validPassword && validConfirmPassword) {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)

                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            startActivity(Intent(this, ProfileActivity::class.java))
                            finish()
                        }
                        else {
                            try {
                                throw task.exception!!
                            } catch (exc: FirebaseAuthUserCollisionException) {
                                binding.containerEmail.helperText = "This email already exist"
                            } catch (exc: FirebaseAuthInvalidCredentialsException) {
                                binding.containerEmail.helperText = "Invalid email format"
                            } catch (exc: Exception) {
                                binding.textViewError.text = "* Registration failed."
                            }
                        }
                    }
            } else {
                binding.textViewError.text = "* Registration failed."
            }

        }

        binding.textViewLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("editTextEmail", binding.editTextEmail.text.toString())
        outState.putString("editTextPassword", binding.editTextPassword.text.toString())
        outState.putString("editTextConfirmPassword", binding.editTextConfirmPassword.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        binding.editTextEmail.setText(savedInstanceState.getString("editTextEmail", ""))
        binding.editTextPassword.setText(savedInstanceState.getString("editTextPassword", ""))
        binding.editTextConfirmPassword.setText(savedInstanceState.getString("editTextConfirmPassword", ""))
    }

}