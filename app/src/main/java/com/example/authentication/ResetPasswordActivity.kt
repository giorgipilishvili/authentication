package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.authentication.databinding.ActivityResetPasswordBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth

class ResetPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityResetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        registerListener()
    }

    private fun registerListener() {
        binding.buttonReset.setOnClickListener {

            val email = binding.editTextEmail.text.toString()

            if (email.isEmpty()) {
                binding.textViewError.text = "* Please enter email."
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        MaterialAlertDialogBuilder(this,
                            R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen)
                            .setMessage("Check email to reset your password.")
                            .setTitle("Reset password")
                            .setPositiveButton("Ok") { dialog, which ->
                                // Respond to positive button press
                            }
                            .show()
                    } else {
                        MaterialAlertDialogBuilder(this,
                            R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen)
                            .setMessage("Check email to reset your password.")
                            .setTitle("Reset password")
                            .setPositiveButton("Ok") { dialog, which ->
                                // Respond to positive button press
                            }
                            .show()
                    }
                }

        }

        binding.textViewBack.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("editTextEmail", binding.editTextEmail.text.toString())

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        binding.editTextEmail.setText(savedInstanceState.getString("editTextEmail", ""))
    }
}