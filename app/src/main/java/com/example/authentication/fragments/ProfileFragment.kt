package com.example.authentication.fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.authentication.R
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment: Fragment (R.layout.fragment_profile) {

    private lateinit var textViewId: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textViewId = view.findViewById(R.id.textViewId)
        textViewId.text = FirebaseAuth.getInstance().currentUser?.uid

    }

}