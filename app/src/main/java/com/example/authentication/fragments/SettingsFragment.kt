package com.example.authentication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.authentication.LoginActivity
import com.example.authentication.PasswordChangeActivity
import com.example.authentication.R
import com.google.firebase.auth.FirebaseAuth

class SettingsFragment: Fragment (R.layout.fragment_settings) {

    private lateinit var buttonLogout: Button
    private lateinit var buttonChangePassword: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonChangePassword = view.findViewById(R.id.buttonPasswordChange)
        buttonLogout = view.findViewById(R.id.buttonLogout)
        registerListeners()

    }

    private fun registerListeners() {
        buttonLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(activity, LoginActivity::class.java))
        }

        buttonChangePassword.setOnClickListener {
            startActivity(Intent(activity, PasswordChangeActivity::class.java))
        }
    }

}