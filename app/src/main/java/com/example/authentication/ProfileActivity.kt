package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.authentication.databinding.ActivityProfileBinding
import com.example.authentication.databinding.ActivityResetPasswordBinding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        val drawerLayout: DrawerLayout = binding.drawerLayout
        binding.burgerButton.setOnClickListener {
            drawerLayout.openDrawer(Gravity.LEFT)
        }

        val burgerNavigationView = findViewById<NavigationView>(R.id.burgerNavigation)
        val controller = findNavController(R.id.nav_host_fragment)

        val appBarConfig = AppBarConfiguration(setOf(
            R.id.profileFragment,
            R.id.settingsFragment
        ))

        setupActionBarWithNavController(controller, appBarConfig)
        burgerNavigationView.setupWithNavController(controller)

    }

}